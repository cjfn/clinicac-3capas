﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapaEntidades
{
    public class Pacientes
    {

        public int idPaciente { get; set; }
        public String nombres { get; set; }
        public String ApPaterno { get; set; }
        public String ApMaterno { get; set; }
        public int edad { get; set; }
        public String sexo { get; set; }
        public String nroDocumento { get; set; }
        public String direccion { get; set; }
        public String telefono { get; set; }
        public bool estado { get; set; }
        public String imagen { get; set; }

        public Pacientes() { }


        public Pacientes(int Id, String Nombre, String ApPaterno, String ApMaterno, int Edad, String Sexo, String Ndocumento, String Direccion, String Telefono, bool Estado, String Imagenes)
        {
            this.idPaciente = Id;
            this.nombres = Nombre;
            this.ApMaterno = ApPaterno;
            this.ApMaterno = ApMaterno;
            this.edad = Edad;
            this.sexo = Sexo;
            this.nroDocumento = Ndocumento;
            this.direccion = Direccion;
            this.telefono = Telefono;
            this.estado = Estado;
            this.imagen = Imagenes;


        }
    }
    
}