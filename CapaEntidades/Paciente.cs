﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CapaEntidades
{
    public class Paciente
    {
        private int IdPaciente;

        public int idPaciente1
        {
            get { return IdPaciente; }
            set { IdPaciente = value; }
        }
        private string Nombres;

        public string Nombres1
        {
            get { return Nombres; }
            set { Nombres = value; }
        }
        private string ApPaterno;

        public string ApPaterno1
        {
            get { return ApPaterno; }
            set { ApPaterno = value; }
        }

        private string ApMaterno;

        public string ApMaterno1
        {
            get { return ApMaterno; }
            set { ApMaterno = value; }
        }
        private int Edad;

        public int Edad1
        {
            get { return Edad; }
            set { Edad = value; }
        }
        private string Sexo;

        public string Sexo1
        {
            get { return Sexo; }
            set { Sexo = value; }
        }
        private string NroDocumento;

        public string NroDocumento1
        {
            get { return NroDocumento; }
            set { NroDocumento = value; }
        }
        private string Direccion;

        public string Direccion1
        {
            get { return Direccion; }
            set { Direccion = value; }
        }
        private string Telefono;

        public string Telefono1
        {
            get { return Telefono; }
            set { Telefono = value; }
        }
        private bool Estado;

        public bool Estado1
        {
            get { return Estado; }
            set { Estado = value; }
        }
        private string imagen;

        public string imagen1
        {
            get { return imagen; }
            set { imagen = value; }
        }
        public Paciente() { }

        public Paciente(int IdPaciente, String Nombres, String ApPaterno, int Edad, String Sexo, String NroDocumento,
            String Direccion, String Telefono, bool Estado, String imagen)
        {
            this.idPaciente1 = IdPaciente;
            this.Nombres1 = Nombres;
            this.ApPaterno1 = ApPaterno;
            this.Edad1 = Edad;
            this.Sexo1 = Sexo;
            this.NroDocumento1 = NroDocumento;
            this.Direccion1 = Direccion;
            this.Telefono1 = Telefono;
            this.Estado1 = Estado;
            this.imagen1 = imagen;
        }

    }
}