﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Script.Serialization;
using CapaEntidades;


namespace CapaPresentacion.webservices
{
    /// <summary>
    /// Descripción breve de PacienteService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
    // [System.Web.Script.Services.ScriptService]
    public class PacienteService : System.Web.Services.WebService
    {

        [WebMethod]
        public void GetPacientes()
        {
            string cs = ConfigurationManager.ConnectionStrings["conn"].ConnectionString;
            List<Pacientes> pacientes = new List<Pacientes>();
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("spListarAllPacientes", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    Pacientes paciente = new Pacientes();
                    paciente.idPaciente = Convert.ToInt32(rdr["idPaciente"]);
                    paciente.nombres = rdr["nombres"].ToString();
                    paciente.ApPaterno = rdr["apPaterno"].ToString();
                    paciente.ApMaterno = rdr["apMaterno"].ToString();
                    paciente.edad =  Convert.ToInt32(rdr["edad"]);
                    paciente.sexo = rdr["sexo"].ToString();
                    paciente.nroDocumento = rdr["nroDocumento"].ToString();
                    paciente.direccion = rdr["direccion"].ToString();
                    paciente.telefono = rdr["telefono"].ToString();
                    paciente.estado = Convert.ToBoolean(rdr["estado"]);
                    paciente.imagen = rdr["imagen"].ToString();
                    pacientes.Add(paciente);

                    
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(pacientes));
           
        }
    }
}
