﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CapaEntidades;
using CapaLogicaNegocio;

namespace CapaPresentacion
{
    public partial class Loguin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnIngresar_Click(object sender, EventArgs e)
        {
            Empleado ObjEmpleado = EmpleadoLN.getInstance().AccesoSistema(txtUsuario.Text, txtPassword.Text);

            if(ObjEmpleado!=null)
            {
                Session["username"] = txtUsuario.Text;
                Response.Redirect("Principal.aspx");
                Response.Write("<script>alert('usuario correcto');</script>");

            }
            else
            {

                Response.Write("<script>alert('usuario incorrecto');</script>");
            }
        }
    }
}