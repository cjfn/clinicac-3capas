﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Home.Master" AutoEventWireup="true" CodeBehind="DetallesPacientes.aspx.cs" Inherits="CapaPresentacion.DetallesPacientes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      
    <section class="content-header">
    <h1 align="center">ADMINISTRA REGISTROS DE CLIENTES</h1>
    </section>
    <section class="content" align="center">
        <div class="row">
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" CssClass="table-bordered" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="idPaciente" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="idPaciente" HeaderText="idPaciente" InsertVisible="False" ReadOnly="True" SortExpression="idPaciente" />
                    <asp:BoundField DataField="nombres" HeaderText="nombres" SortExpression="nombres" />
                    <asp:BoundField DataField="apPaterno" HeaderText="apPaterno" SortExpression="apPaterno" />
                    <asp:BoundField DataField="apMaterno" HeaderText="apMaterno" SortExpression="apMaterno" />
                    <asp:BoundField DataField="edad" HeaderText="edad" SortExpression="edad" />
                    <asp:BoundField DataField="sexo" HeaderText="sexo" SortExpression="sexo" />
                    <asp:BoundField DataField="nroDocumento" HeaderText="nroDocumento" SortExpression="nroDocumento" />
                    <asp:BoundField DataField="direccion" HeaderText="direccion" SortExpression="direccion" />
                    <asp:BoundField DataField="telefono" HeaderText="telefono" SortExpression="telefono" />
                    <asp:CheckBoxField DataField="estado" HeaderText="estado" SortExpression="estado" />
                    <asp:BoundField DataField="imagen" HeaderText="imagen" SortExpression="imagen" />
                </Columns>
               
            </asp:GridView>
            
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:conn %>" DeleteCommand="DELETE FROM [Paciente] WHERE [idPaciente] = @idPaciente" InsertCommand="INSERT INTO [Paciente] ([nombres], [apPaterno], [apMaterno], [edad], [sexo], [nroDocumento], [direccion], [telefono], [estado], [imagen]) VALUES (@nombres, @apPaterno, @apMaterno, @edad, @sexo, @nroDocumento, @direccion, @telefono, @estado, @imagen)" SelectCommand="SELECT * FROM [Paciente]" UpdateCommand="UPDATE [Paciente] SET [nombres] = @nombres, [apPaterno] = @apPaterno, [apMaterno] = @apMaterno, [edad] = @edad, [sexo] = @sexo, [nroDocumento] = @nroDocumento, [direccion] = @direccion, [telefono] = @telefono, [estado] = @estado, [imagen] = @imagen WHERE [idPaciente] = @idPaciente">
                <DeleteParameters>
                    <asp:Parameter Name="idPaciente" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="nombres" Type="String" />
                    <asp:Parameter Name="apPaterno" Type="String" />
                    <asp:Parameter Name="apMaterno" Type="String" />
                    <asp:Parameter Name="edad" Type="Int32" />
                    <asp:Parameter Name="sexo" Type="String" />
                    <asp:Parameter Name="nroDocumento" Type="String" />
                    <asp:Parameter Name="direccion" Type="String" />
                    <asp:Parameter Name="telefono" Type="String" />
                    <asp:Parameter Name="estado" Type="Boolean" />
                    <asp:Parameter Name="imagen" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="nombres" Type="String" />
                    <asp:Parameter Name="apPaterno" Type="String" />
                    <asp:Parameter Name="apMaterno" Type="String" />
                    <asp:Parameter Name="edad" Type="Int32" />
                    <asp:Parameter Name="sexo" Type="String" />
                    <asp:Parameter Name="nroDocumento" Type="String" />
                    <asp:Parameter Name="direccion" Type="String" />
                    <asp:Parameter Name="telefono" Type="String" />
                    <asp:Parameter Name="estado" Type="Boolean" />
                    <asp:Parameter Name="imagen" Type="String" />
                    <asp:Parameter Name="idPaciente" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <asp:EntityDataSource ID="EntityDataSource1" runat="server">
            </asp:EntityDataSource>
            
        </div>
        <div class="row">
            <div>

           <div class="table-responsive">

                  <table class="table table-hover table-bordered" id="MyTable">

                    <thead>

                      <tr>

                        <th>

                          idPaciente

                        </th>

                        <th>

                          nombres


                        </th>

                        <th>

                          apPaterno 

                        </th>

                        <th>

                          apMaterno

                        </th>

                           <th>

                          edad

                        </th>

                           <th>

                          sexo

                        </th>

                           <th>

                          nroDocumento

                        </th>

                           <th>

                          direccion

                        </th>

                           <th>

                          telefono

                        </th>

                           <th>

                          estado

                        </th>

                           <th>

                          imagen

                        </th>

                      </tr>

                    </thead>

                  



                  </table>


                </div>  
            
               <link href="css/dataTables.bootstrap.min.css" rel="stylesheet" />
       <link rel="stylesheet" type="text/css" href="http://cdn.datatables.net/1.10.15/css/jquery.dataTables.css">
       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
       <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.js"></script>
       <script type="text/javascript">

           $(document).ready(function () {
               $.ajax({
                   url: '/webservices/PacienteService.asmx/GetPacientes',
                   method: 'post',
                   dataType: 'json',
                   success: function (data) {
                       $('#MyTable').dataTable({
                           data: data,
                           columns: [
                              { 'data': 'idPaciente' },
                              { 'data': 'nombres' },
                              { 'data': 'apPaterno' },
                              { 'data': 'apMaterno' },
                              { 'data': 'edad' },
                              { 'data': 'sexo' },
                              { 'data': 'nroDocumento' },
                              { 'data': 'direccion' },
                              { 'data': 'telefono' },
                              { 'data': 'estado' },
                              { 'data': 'imagen' }
                           ]
                       });
                   }
               });
           });
       </script>
             
            </div>
            </div>
    </section>
</asp:Content>
