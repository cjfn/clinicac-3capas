﻿
function addRowDT(data) {
    var tabla =$('#tbl_pacientes').DataTable();
    for(var i=0; i<data.length; i++){
        tabla.fnAddData([
            data.codigo,
            data.nombre,
            data.apellidos,
            data.sexo,
            data.edad,
            data.direccion,
            data.estado
        ]);
    }
}

function sendDataAjax() {
    $.ajax({
        type: "POST",
        url: "DetallesPacientes.aspx/ListarPacientes",
        data: {},
        contentType: 'application/json; charset=utf-8',
        error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status + " \n" + xhr.responseText, "\n" + thrownError);

        },
        success: function (data) {
            console.log(data);
            //addRowDt.data
        }
    });
}

//llamado a la funcion ajax al cargar el documento

sendDataAjax();