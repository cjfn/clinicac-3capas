﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CapaAccesoDatos;
using CapaEntidades;
using System.Threading.Tasks;

namespace CapaLogicaNegocio
{
    public class PacienteLN
    {
        public PacienteLN()
        { }

        public int IngresoPacientes(string nombres, string apPaterno, string apMaterno, int edad, string sexo, 
            string nrodocumento, string direccion, string telefono, int estado)
        {
            PacienteDAO DatPaciente = new PacienteDAO();
            return DatPaciente.insertPaciente(nombres, apPaterno, apMaterno, edad, sexo, nrodocumento, direccion, telefono, estado);
        }

        public List<Paciente> select_all_pacientes()
        {
            try
            {
                return select_all_pacientes();
            }
            catch(Exception e)
            {
                throw e;
            }
        }

    }
}