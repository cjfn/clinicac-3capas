﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data.Common;
using System.Data;
using CapaEntidades;
using CapaEntidades.MiclinicaTableAdapters;
using System.Data.SqlClient;


namespace CapaAccesoDatos
{
    public class PacienteDAO
    {
        public PacienteDAO() { }
        public static string constr
        {
            get { return ConfigurationManager.ConnectionStrings["conn"].ConnectionString; }
        }

        public static string Provider //proveedor de diferentes tipos de bases de datos
        {
            get { return ConfigurationManager.ConnectionStrings["conn"].ProviderName; }
        }

        public static DbProviderFactory dpf
        {
            get { return DbProviderFactories.GetFactory(Provider); }
        }

        public static int ejecutaNonQuery(string StoredProcedure, List<DbParameter> parametros)
        {
            int Id = 0;
            try
            {
                using (DbConnection con = dpf.CreateConnection())
                {
                    con.ConnectionString = constr;

                    using (DbCommand cmd = dpf.CreateCommand())
                    {
                        cmd.Connection = con;
                        cmd.CommandText = StoredProcedure;
                        cmd.CommandType = CommandType.StoredProcedure;

                        foreach (DbParameter param in parametros)
                            cmd.Parameters.Add(param);
                        con.Open();
                        Id = cmd.ExecuteNonQuery();

                    }


                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                //coneccion cerrada
            }
            return Id;
        }

        public int insertPaciente(string nombres, string apPaterno, string apMaterno, int edad, string sexo, 
            string nrodocumento, string direccion, string telefono, int estado )
        {
            List<DbParameter> parametros = new List<DbParameter>(); ;

            DbParameter param1 = dpf.CreateParameter();
            param1.Value = nombres;
            param1.ParameterName = "nombres";
            parametros.Add(param1);

            DbParameter param2 = dpf.CreateParameter();
            param2.Value = apPaterno;
            param2.ParameterName = "apPaterno";
            parametros.Add(param2);

            DbParameter param3 = dpf.CreateParameter();
            param3.Value = apMaterno;
            param3.ParameterName = "apMaterno";
            parametros.Add(param3);



            DbParameter param4 = dpf.CreateParameter();
            param4.Value = edad;
            param4.ParameterName = "edad";
            parametros.Add(param4);


            DbParameter param5 = dpf.CreateParameter();
            param5.Value = sexo;
            param5.ParameterName = "sexo";
            parametros.Add(param5);


            DbParameter param6 = dpf.CreateParameter();
            param6.Value = nrodocumento;
            param6.ParameterName = "nroDocumento";
            parametros.Add(param6);

            DbParameter param7 = dpf.CreateParameter();
            param7.Value = direccion;
            param7.ParameterName = "direccion";
            parametros.Add(param7);

            DbParameter param8 = dpf.CreateParameter();
            param8.Value = telefono;
            param8.ParameterName = "telefono";
            parametros.Add(param8);


            DbParameter param9 = dpf.CreateParameter();
            param9.Value = estado;
            param9.ParameterName = "estado";
            parametros.Add(param9);




            return ejecutaNonQuery("spinsertar_Paciente", parametros);



        }

        public List<Paciente> select_all_pacientes()
        {
            List<Paciente> LstPaciente = new List<Paciente>();

            string StoredProcedure = "spListarPacientes";
            using (DbConnection con = dpf.CreateConnection())
            {
                con.ConnectionString = constr;
                using (DbCommand cmd = dpf.CreateCommand())
                {
                    cmd.Connection = con;
                    cmd.CommandText = StoredProcedure;
                    cmd.CommandType = CommandType.StoredProcedure;
                    con.Open();
                    using (DbDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            //crear objeto tipo paciente
                            Paciente ObjPaciente = new Paciente();
                            ObjPaciente.idPaciente1 = Convert.ToInt32(dr["idPaciente"].ToString());
                            ObjPaciente.Nombres1 = (dr["nombreS"].ToString());
                            ObjPaciente.ApPaterno1 = (dr["apPaterno"].ToString());
                            ObjPaciente.ApMaterno1 = (dr["apMaterno"].ToString());
                            ObjPaciente.Edad1 = Convert.ToInt32(dr["edad"].ToString());
                            ObjPaciente.Sexo1 = (dr["sexo"].ToString());
                            ObjPaciente.NroDocumento1 = (dr["nroDocumento"].ToString());
                            ObjPaciente.Direccion1 = (dr["idPaciente"].ToString());
                            ObjPaciente.Estado1 = true;

                            // añadir a la lista de objetso
                            LstPaciente.Add(ObjPaciente);
                        }
                    }
                }



            }
            return LstPaciente;


        }

       
    }
}