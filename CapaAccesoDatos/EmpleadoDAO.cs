﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CapaEntidades;
using System.Data;
using System.Text;
using System.Data.SqlClient;

namespace CapaAccesoDatos
{
    public class EmpleadoDAO
    {
#region "PATRON SINGLETON"
        private static EmpleadoDAO daoEmpleado = null;
        private EmpleadoDAO(){}
        public static EmpleadoDAO getInstance()
        {
            if (daoEmpleado==null)
            {
                daoEmpleado = new EmpleadoDAO();
            }
            return daoEmpleado;
        }
#endregion
        public Empleado AccesoSistema(string user, String  pass)
        {
            SqlConnection conexion = null;
            SqlCommand cmd = null;
            Empleado objEmpleado = null; //objeto empleado
            SqlDataReader dr = null;
            try
            {
                conexion = Conexion.getInstance().ConexionDB();// conexion
                cmd = new SqlCommand("spAccesoSistema", conexion);// nombre del comando
                cmd.CommandType = CommandType.StoredProcedure;//tipo de comando
                cmd.Parameters.AddWithValue("@prmUser", user);
                cmd.Parameters.AddWithValue("@prmPass", pass);// parametros
                conexion.Open();
                dr = cmd.ExecuteReader();
                if(dr.Read())
                {
                    objEmpleado = new Empleado();
                    objEmpleado.ID = Convert.ToInt32(dr["idEmpleado"].ToString());
                    objEmpleado.Usuario = dr["usuario"].ToString();
                    objEmpleado.Clave = dr["clave"].ToString();
                }

            }
            catch
                (Exception ex)
            {
                objEmpleado = null;
                throw ex;
            }

            finally
            {
                conexion.Close();
            }

            return objEmpleado;
        }
    }
}